package soldierplayer;

import battlecode.common.*;

public class RobotPlayer {


	public static void run(RobotController myRC) {

		while (true) {
			try {
				while (!myRC.getType().equals(RobotType.ARCHON) && myRC.getFlux() <= 5) {
					myRC.yield();
				}
				
				RobotType unit = RobotType.SOLDIER;

				MapLocation front = myRC.getLocation().add(myRC.getDirection());
				GameObject frontObject = myRC.senseObjectAtLocation(front, unit.level);
				//decide flux transfer
				if (myRC.getType().equals(RobotType.ARCHON) && frontObject != null && frontObject instanceof Robot
						&& !myRC.senseRobotInfo((Robot)frontObject).type.equals(RobotType.ARCHON)
						&& !myRC.senseRobotInfo((Robot)frontObject).type.equals(RobotType.TOWER)
						&& frontObject.getTeam() == myRC.getTeam())
				{
					if (!myRC.isMovementActive())
					{
						myRC.setDirection(myRC.getDirection().rotateRight());
					}
					myRC.transferFlux(front, unit.level, myRC.getFlux());
				}

				//decide attack
				if (!myRC.isAttackActive() && myRC.getType() != RobotType.ARCHON)
				{
					Robot[] bots = myRC.senseNearbyGameObjects(Robot.class);
					MapLocation att = null;
					RobotLevel attr = null;
					for (int i = 0; i < bots.length; i++)
					{
						if (bots[i].getTeam() != myRC.getTeam()
								&& myRC.canAttackSquare((myRC.senseRobotInfo(bots[i]).location)))
						{
							att = myRC.senseRobotInfo(bots[i]).location;
							attr = myRC.senseRobotInfo(bots[i]).type.level;
						}
						
							
					}
					if (att != null)
					{
						myRC.attackSquare(att, attr);
					}
				}
				//spawn counts as movement
				if (!myRC.isMovementActive()) //you can attack while moving
				if (myRC.getType().equals(RobotType.ARCHON) && myRC.getFlux() >= unit.spawnCost+unit.maxFlux
						&& myRC.senseTerrainTile(front) == TerrainTile.LAND
						&& myRC.senseObjectAtLocation(front, unit.level) == null)
				{
					myRC.spawn(unit);
				}
				else if (myRC.canMove(myRC.getDirection())) {
					myRC.moveForward();
				}
				else {
					myRC.setDirection(myRC.getDirection().rotateLeft());
				}

				myRC.yield();
			} catch (Exception e) {
				System.out.println("caught exception:");
				e.printStackTrace();
			}
		}
	}
}
