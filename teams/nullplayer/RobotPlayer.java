package nullplayer;

import battlecode.common.*;

public class RobotPlayer {

    public static void run(RobotController myRC) {
        while (true) {
            try {
                myRC.yield();
            } catch (Exception e) {
                System.out.println("caught exception:");
                e.printStackTrace();
            }
        }
    }
}
