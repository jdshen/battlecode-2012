package team053.state;

public enum MoveState {
	PATROL, BUGGING, SCOUTING, FIGHTING, RETREATING, PUSHING, IDLE
}
