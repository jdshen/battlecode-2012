package team053.state;

public enum MessageState {
	ENEMY,
	KITE,
	PUSH,
	NO_ENEMIES,
	RALLY,
	DEFEND,
	SPAWN,
	TOWER,
	EXPAND,
	SPLIT;
}
