package team053.state;

public enum GameState {
	SPLIT, SPAWN, EXPAND, TOWERS, DEFEND
}
