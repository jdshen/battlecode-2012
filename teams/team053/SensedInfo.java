package team053;
import battlecode.common.*;
public class SensedInfo {
	public FastRobotInfo[] nearby;
	public FastRobotInfo[] enemies;
	public FastRobotInfo[] allies;
	public MapLocation[] archons;
	public PowerNode[] alliedNodes;
	public MapLocation[] capturable;
	public PowerNode[] nearbyNodes;
}
