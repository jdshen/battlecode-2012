package team053;
import battlecode.common.*;

public class FastRobotInfo {
	public Robot r;
	public RobotInfo info;
	public int round;
	
	public FastRobotInfo(Robot r, RobotInfo info, int round)
	{
		this.r = r;
		this.info = info;
		this.round = round;
	}
}
