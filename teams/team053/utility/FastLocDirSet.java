package team053.utility;
import battlecode.common.*;

public class FastLocDirSet {
    private static final int HASH = Math.max(GameConstants.MAP_MAX_WIDTH, GameConstants.MAP_MAX_HEIGHT);
    private int size = 0;
    private boolean[][][] has = new boolean[HASH][HASH][8];

    public void add(MapLocation loc, Direction dir) {
        int x = loc.x % HASH;
        int y = loc.y % HASH;
        int z = dir.ordinal() % 8;
        if (!has[x][y][z]){
            size++;
            has[x][y][z] = true;
        }
    }

    public void remove(MapLocation loc, Direction dir) {
        int x = loc.x % HASH;
        int y = loc.y % HASH;
        int z = dir.ordinal() % 8;
        if (has[x][y][z]){
            size--;
            has[x][y][z] = false;
        }
    }

    public boolean contains(MapLocation loc, Direction dir) {
        return has[loc.x % HASH][loc.y % HASH][dir.ordinal() % 8];
    }

    public void clear() {
        has = new boolean[HASH][HASH][8];
        size = 0;
    }
    
    public int size()
    {
    	return size;
    }
}