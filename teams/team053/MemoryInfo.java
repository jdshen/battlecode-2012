package team053;

import battlecode.common.*;
import team053.utility.*;

public class MemoryInfo {
	public PowerNode core;
	public FastLocSet nodeLoc;
	public FastLocSet expandLoc;
	public FastLocSet defendLoc;

	public MemoryInfo(PowerNode core)
	{
		this.core = core;
		nodeLoc = new FastLocSet();
		expandLoc = new FastLocSet();
		defendLoc = new FastLocSet();
	}
}
