package team053;
import battlecode.common.*;
import team053.controller.AttackController;
import team053.controller.FluxController;
import team053.controller.MessageController;
import team053.controller.NavigationController;
import team053.controller.RegenController;
import team053.controller.StateController;
import team053.state.*;

public class RobotState {
	public RobotController rc;
	public MessageController mc;
	public FluxController fc;
	public RegenController hc;
	public AttackController ac;
	public NavigationController nc;
	public StateController sc;
	public SensedInfo info;
	public MemoryInfo mem;
	public GameState gstate;
	public FightState fstate;
	public MoveState mstate;
	
	public RobotState(RobotController rc)
	{
		this.rc = rc;
		mc = new MessageController(this);
		fc = new FluxController(this);
		hc = new RegenController(this);
		ac = new AttackController(this);
		nc = new NavigationController(this);
		sc = new StateController(this);
		info = new SensedInfo();
		this.mem = new MemoryInfo(rc.sensePowerCore());
		if (this.rc.getType() == RobotType.ARCHON)
			gstate = GameState.SPLIT;
		else gstate = GameState.SPAWN;
		fstate = FightState.DOGFIGHT;
		mstate = MoveState.IDLE;
	}
}
