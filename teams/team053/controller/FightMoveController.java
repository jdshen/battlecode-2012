package team053.controller;

import battlecode.common.*;
import team053.*;
import team053.state.*;


public class FightMoveController 
{
	public RobotState rs;
	public boolean hasActed;

	public FastRobotInfo[] nearbyUnits;
	public FastRobotInfo[] nearbyEnemies;
	public static final int ARCHON_RUN = 25;
	public int queueLocation;

	public FightMoveController(RobotState rs)
	{
		this.rs = rs;
	}
	
	public void startFight()
	{
		queueLocation = 0;
	}

	public void run() throws GameActionException
	{
		nearbyUnits = rs.info.nearby;
		nearbyEnemies = rs.info.enemies; 

		hasActed=false;
		
		if(queueLocation==-1 && rs.rc.canMove(rs.rc.getDirection().opposite()))
		{
			rs.rc.moveBackward();
			queueLocation = 0;
		}
		else if (queueLocation==1 && rs.rc.canMove(rs.rc.getDirection()))
		{
			rs.rc.moveForward();
			queueLocation = 0;
		}
		else
		{
			if (rs.rc.getType()==RobotType.ARCHON)
			{
				archon();
				return;
			}
			//END ARCHON
			queueLocation = 0;
			if (rs.fstate == FightState.DOGFIGHT)
			{
				moveBackwards();
				
				//check if any units are attackable
				checkAttackable();
	
				
				if (!hasActed)
				{
					turn();
				}
				if (!hasActed)
				{
					moveForwards();
				}
				if (!hasActed)
				{
					turnAndMoveForwards();
				}
			}
			else if (rs.fstate == FightState.PUSH)
			{
				checkAttackable();
				if (!hasActed)
					turn();
				if(!hasActed)
					moveForwards();
				if(!hasActed)
					turnAndMoveForwards();
			}
			else if (rs.fstate == FightState.KITE)
			{
				kite();
			}
		}
	}
	
	public void archon() throws GameActionException
	{
		int nearestEnemy = 1000;
		FastRobotInfo target=null;
		for (FastRobotInfo x: rs.info.enemies)
		{
			int d = rs.rc.getLocation().distanceSquaredTo(x.info.location);
			if (d<nearestEnemy && d > 0)
			{
				target=x;
				nearestEnemy = d;
			}
		}
		if (target!=null && nearestEnemy<=ARCHON_RUN)
		{
			if (rs.rc.canMove(rs.rc.getLocation().directionTo(target.info.location).opposite()))
			{
				if (rs.rc.getDirection()==rs.rc.getLocation().directionTo(target.info.location))
				{
					rs.rc.moveBackward();
				}
				else
				{
					rs.rc.setDirection(rs.rc.getLocation().directionTo(target.info.location));
					queueLocation=-1;
				}
				return;

			}
			else if (rs.rc.canMove(rs.rc.getLocation().directionTo(target.info.location).rotateLeft().opposite()))
			{
				if (rs.rc.getDirection()==rs.rc.getLocation().directionTo(target.info.location).rotateLeft())
				{
					rs.rc.moveBackward();
				}
				else
				{
					rs.rc.setDirection(rs.rc.getLocation().directionTo(target.info.location).rotateLeft());
					queueLocation=-1;
				}
				return;
			}
			else if (rs.rc.canMove(rs.rc.getLocation().directionTo(target.info.location).rotateRight().opposite()))
			{
				if (rs.rc.getDirection()==rs.rc.getLocation().directionTo(target.info.location).rotateRight())
				{
					rs.rc.moveBackward();
				}
				else
				{
					rs.rc.setDirection(rs.rc.getLocation().directionTo(target.info.location).rotateRight());
					queueLocation=-1;
				}
				return;
			}
			else if (rs.rc.canMove(rs.rc.getLocation().directionTo(target.info.location).rotateRight().rotateRight().opposite()))
			{
				if (rs.rc.getDirection()==rs.rc.getLocation().directionTo(target.info.location).rotateRight().rotateRight())
				{
					rs.rc.moveBackward();
				}
				else
				{
					rs.rc.setDirection(rs.rc.getLocation().directionTo(target.info.location).rotateRight().rotateRight());
					queueLocation=-1;
				}
				return;
			}
			else if (rs.rc.canMove(rs.rc.getLocation().directionTo(target.info.location).rotateLeft().rotateLeft().opposite()))
			{
				if (rs.rc.getDirection()==rs.rc.getLocation().directionTo(target.info.location).rotateLeft().rotateLeft())
				{
					rs.rc.moveBackward();
				}
				else
				{
					rs.rc.setDirection(rs.rc.getLocation().directionTo(target.info.location).rotateLeft().rotateLeft());
					queueLocation=-1;
				}
				return;
			}
			else if (rs.rc.canMove(rs.rc.getLocation().directionTo(target.info.location).rotateLeft()))
			{
				if (rs.rc.getDirection()==rs.rc.getLocation().directionTo(target.info.location).rotateLeft())
				{
					rs.rc.moveForward();
				}
				else
				{
					rs.rc.setDirection(rs.rc.getLocation().directionTo(target.info.location).rotateLeft());
					queueLocation=1;
				}
				return;
			}
			else if (rs.rc.canMove(rs.rc.getLocation().directionTo(target.info.location).rotateLeft()))
			{
				if (rs.rc.getDirection()==rs.rc.getLocation().directionTo(target.info.location).rotateLeft())
				{
					rs.rc.moveForward();
				}
				else
				{
					rs.rc.setDirection(rs.rc.getLocation().directionTo(target.info.location).rotateLeft());
					queueLocation=1;
				}
				return;
			}
			else if (rs.rc.canMove(rs.rc.getLocation().directionTo(target.info.location).rotateRight()))
			{
				if (rs.rc.getDirection()==rs.rc.getLocation().directionTo(target.info.location).rotateRight())
				{
					rs.rc.moveForward();
				}
				else
				{
					rs.rc.setDirection(rs.rc.getLocation().directionTo(target.info.location).rotateRight());
					queueLocation=1;
				}
				return;
			}
		}
	}
	
	public void checkAttackable()
	{
		for (FastRobotInfo enemy: nearbyEnemies)
		{
			if (rs.rc.canAttackSquare(enemy.info.location))
			{
				hasActed = true;
				return;
			}
		}	
	}
	
	public void kite() throws GameActionException
	{
		if (rs.rc.canMove(rs.rc.getDirection().opposite()))
		{
			for (FastRobotInfo enemy: nearbyEnemies)
			{
				if (rs.rc.getLocation().distanceSquaredTo(enemy.info.location)<=rs.rc.getType().attackRadiusMaxSquared)
				{
					rs.rc.moveBackward();
					hasActed=true;
					break;
				}
			}
		}
		else if (rs.rc.canMove(rs.rc.getDirection().rotateLeft().opposite()))
		{
			for (FastRobotInfo enemy: nearbyEnemies)
			{
				if (rs.rc.getLocation().distanceSquaredTo(enemy.info.location)<=rs.rc.getType().attackRadiusMaxSquared)
				{
					hasActed=true;
					rs.rc.setDirection(rs.rc.getDirection().rotateLeft());
					queueLocation=-1;
					return;
				}
			}			
		}
		else if (rs.rc.canMove(rs.rc.getDirection().rotateRight().opposite()))
		{
			for (FastRobotInfo enemy: nearbyEnemies)
			{
				if (rs.rc.getLocation().distanceSquaredTo(enemy.info.location)<=rs.rc.getType().attackRadiusMaxSquared)
				{
					hasActed=true;
					rs.rc.setDirection(rs.rc.getDirection().rotateRight());
					queueLocation=-1;
					return;
				}
			}	
		}
	}

	public void moveBackwards() throws GameActionException
	{
		if(rs.rc.canMove(rs.rc.getDirection().opposite()))
		{
			for (FastRobotInfo enemy: nearbyEnemies)
			{
				if (rs.rc.canAttackSquare(enemy.info.location.add(rs.rc.getDirection())))
				{
					hasActed=true;
					rs.rc.moveBackward();
					queueLocation = 0;
					return;
				}
			}
		}
		else if (rs.rc.canMove(rs.rc.getDirection().rotateLeft().opposite()))
		{
			for (FastRobotInfo enemy: nearbyEnemies)
			{
				if (rs.rc.canAttackSquare(enemy.info.location.add(rs.rc.getDirection())))
				{
					hasActed=true;
					rs.rc.setDirection(rs.rc.getDirection().rotateLeft());
					queueLocation=-1;
					return;
				}
			}			
		}
		else if (rs.rc.canMove(rs.rc.getDirection().rotateRight().opposite()))
		{
			for (FastRobotInfo enemy: nearbyEnemies)
			{
				if (rs.rc.canAttackSquare(enemy.info.location.add(rs.rc.getDirection())))
				{
					hasActed=true;
					rs.rc.setDirection(rs.rc.getDirection().rotateRight());
					queueLocation=-1;
					return;
				}
			}	
		}
	}
	public void turn() throws GameActionException
	{
		FastRobotInfo target = null;
		Direction targetDirection = null;
		
		for (FastRobotInfo enemy: nearbyEnemies)
		{
			if (rs.rc.getLocation().distanceSquaredTo(enemy.info.location)<=rs.rc.getType().attackRadiusMaxSquared)
			{
				if (target == null || target.info.type == RobotType.TOWER)
					target = enemy;
				else if (target.info.type == RobotType.ARCHON && enemy.info.type!=RobotType.TOWER)
					target=enemy;
				
				targetDirection = Bugger.direction(rs.rc.getLocation(), target.info.location);

				hasActed=true;
			}
		}
		if (targetDirection!=null && targetDirection!=Direction.OMNI)
		{
			rs.rc.setDirection(targetDirection);
			hasActed=true;
		}
	}
	
	public void moveForwards() throws GameActionException
	{
		if(rs.rc.canMove(rs.rc.getDirection()))
		{
			for (FastRobotInfo enemy: nearbyEnemies)
			{
				if (rs.rc.canAttackSquare(enemy.info.location.subtract(rs.rc.getDirection())))
				{
					hasActed=true;
					rs.rc.moveForward();
					queueLocation = 0;
					return;
				}
			}				
		}
		else if (rs.rc.canMove(rs.rc.getDirection().rotateLeft()))
		{
			for (FastRobotInfo enemy: nearbyEnemies)
			{
				if (rs.rc.canAttackSquare(enemy.info.location.subtract(rs.rc.getDirection())))
				{
					hasActed=true;
					rs.rc.setDirection(rs.rc.getDirection().rotateLeft());
					queueLocation=1;
					return;
				}
			}			
		}
		else if (rs.rc.canMove(rs.rc.getDirection().rotateRight()))
		{
			for (FastRobotInfo enemy: nearbyEnemies)
			{
				if (rs.rc.canAttackSquare(enemy.info.location.subtract(rs.rc.getDirection())))
				{
					hasActed=true;
					rs.rc.setDirection(rs.rc.getDirection().rotateRight());
					queueLocation=1;
					return;
				}
			}	
		}
	}
	public void turnAndMoveForwards() throws GameActionException
	{
		FastRobotInfo target = null;
		Direction targetDirection = null;
		
		for (FastRobotInfo enemy: nearbyEnemies)
		{
			if (rs.rc.getLocation().distanceSquaredTo(enemy.info.location)>rs.rc.getType().attackRadiusMaxSquared)
			{
				if (target == null || target.info.type == RobotType.TOWER)
					target = enemy;
				else if (target.info.type == RobotType.ARCHON && enemy.info.type!=RobotType.TOWER)
					target=enemy;
				
				targetDirection = Bugger.direction(rs.rc.getLocation(), target.info.location);
			}
		}
		if (targetDirection!=null)
		{
			rs.rc.setDirection(targetDirection);
			hasActed=true;
		}
	}
}
