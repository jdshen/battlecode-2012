package team053.controller;
import team053.*;
import team053.state.*;
import battlecode.common.*;

public class StateController {
	public RobotState rs;
	public int lastEnemy;
	public static final int TOWER_ROUND = 1200;
	
	public StateController(RobotState rs)
	{
		this.rs = rs;
		this.lastEnemy = 0;
	}

	public void run()
	{
		if (rs.rc.getType() == RobotType.ARCHON)
		{
			if(rs.mstate == MoveState.IDLE)
			{
				if (rs.gstate == GameState.SPLIT && Clock.getRoundNum() != 0)
				{
					rs.nc.sc.done_spawning = false;
					rs.gstate = GameState.EXPAND;
				}
				else if (rs.gstate == GameState.EXPAND)
				{
					rs.gstate = GameState.EXPAND;
				}
				else if (rs.gstate == GameState.SPAWN && rs.nc.sc.done_spawning)
				{
					rs.gstate = GameState.EXPAND;
					rs.nc.sc.done_spawning = false;
				}
				else if (rs.gstate == GameState.DEFEND)
				{
					rs.gstate = GameState.SPAWN;
				}
			}
			else if (rs.gstate == GameState.SPLIT)
			{
				int closest = 10000;
				for (int i = 0; i < rs.info.archons.length; i++)
				{
					int d = rs.rc.getLocation().distanceSquaredTo(rs.info.archons[i]);
					if (d != 0 && d < closest)
					{
						closest = d;
					}
				}
//				if (closest >= GameConstants.BROADCAST_RADIUS_SQUARED)
//					rs.mstate = MoveState.IDLE;
				if (!rs.mem.nodeLoc.contains(rs.nc.mc.to) || rs.nc.mc.to.equals(rs.mem.core.getLocation()))
				{
					if (closest >= GameConstants.PRODUCTION_PENALTY_R2 || Clock.getRoundNum() > 60) //take about 60 rounds to split
					{
						rs.mstate = MoveState.IDLE;
					}
				}
				else if (rs.rc.getFlux() >= 300)
				{
					rs.mstate = MoveState.IDLE;
					rs.gstate = GameState.SPAWN;
				}
//				else if (Clock.getRoundNum() > 150) //if still havent found power node in 150 rounds, stop 
//				{
//					rs.mstate = MoveState.IDLE;
//				}
			}
			else if (rs.rc.getFlux() >= 300)
			{
				rs.mstate = MoveState.IDLE;
				rs.gstate = GameState.SPAWN;
			}
		}
		else
		{
		}
	
		if (rs.gstate == GameState.TOWERS)
		{
			if (rs.nc.mc.to != null)
				for (int i = 0; i < rs.info.alliedNodes.length; i++)
				{
					if (rs.nc.mc.to.equals(rs.info.alliedNodes[i].getLocation()))
						rs.mstate = MoveState.IDLE;
				}
		}
		lastEnemy++;
		
		boolean nearbyEnemies = false;
		for (int i = 0; i < rs.info.enemies.length; i++)
		{
			if (rs.info.enemies[i].info.type != RobotType.TOWER
					&& rs.info.enemies[i].info.flux > GameConstants.UNIT_UPKEEP)
				nearbyEnemies =true;
		}
		
		if (nearbyEnemies)
		{
			lastEnemy = 0;
			if (rs.mstate != MoveState.FIGHTING)
			{
//				if (rs.rc.getType() == RobotType.ARCHON)
//				{
////					if (rs.gstate == GameState.TOWERS)
//						rs.gstate = GameState.SPAWN;
//				}
//				else
					rs.gstate = GameState.EXPAND;
				rs.mstate = MoveState.FIGHTING;
				rs.fstate = FightState.DOGFIGHT;
				rs.nc.fc.startFight();
				rs.nc.mc.to = rs.info.enemies[0].info.location;
			}
			
		}
		else
		{
			if (rs.mstate == MoveState.FIGHTING)
			{
				if (rs.rc.getType() != RobotType.ARCHON)
				{
					rs.gstate = GameState.DEFEND;
					rs.mstate = MoveState.PUSHING;
					rs.nc.mc.from = rs.rc.getLocation();
					rs.nc.mc.startBug();
				}
				else
				{
					rs.gstate = GameState.DEFEND;
					rs.mstate = MoveState.IDLE;
				}
			}
		}
		
		if (rs.rc.getType() != RobotType.ARCHON && rs.mstate != MoveState.RETREATING)
		{
			if (rs.rc.getFlux() < FluxController.RETREAT_FLUX*rs.rc.getType().moveCost)
			{
				int closest = 10000;
				int min = -1;
				for (int i = 0; i < rs.info.archons.length; i++)
				{
					int d = rs.rc.getLocation().distanceSquaredTo(rs.info.archons[i]) ;
					if (d < closest)
					{
						closest = d;
						min = i;
					}
				}
				if (min != -1)
				{
					rs.nc.mc.from = rs.rc.getLocation();
					rs.nc.mc.to = rs.info.archons[min];
					rs.nc.mc.startBug();
					rs.mstate = MoveState.RETREATING;
					rs.fstate = FightState.KITE;
				}
			}
		}

		int round = Clock.getRoundNum();

		if (rs.gstate != GameState.TOWERS)
			if (round >= TOWER_ROUND && lastEnemy*1.0 >= ((1.0)*TOWER_ROUND*TOWER_ROUND*TOWER_ROUND)/round/round)
			{
				rs.gstate = GameState.TOWERS;
				if (rs.rc.getType() == RobotType.ARCHON)
					rs.nc.tc.towers();
			}
	}
}
