package team053.controller;
import team053.*;
import team053.state.*;
import battlecode.common.*;

public class SpawnController {
	public RobotState rs;
	
	public boolean done_spawning = false;

	public static final int MAX_UNITS = 500;
	public static final double DECAY = Math.pow(.5, 1/200.0);
	public static final double[] FLUX_CYCLE = new double[]{100, 200, 300, 1000, 1700, 2170, 2530, 2830, 3100, 3300, 3500, 3700,
		3900, 4100, 10000};
	public int fluxCounter = 0;
	public static final RobotType[] UNITS = RobotType.values();
		//ARCHON, SOLDIER, SCOUT, DISRUPTER, SCORCHER, TOWER
	public static final double[][] COUNTER =
			new double[][]{
		new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 0.0},
		new double[]{0.0, 0.3, 0.1, 0.0, 0.6, 0.0},
		new double[]{0.0, 0.3, 0.2, 0.5, 0.0, 0.0},
		new double[]{0.0, 0.8, 0.0, 0.2, 0.0, 0.0},
		new double[]{0.0, 0.0, 0.1, 0.7, 0.2, 0.0},
		new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 0.0}
	};
	public double fluxUsed = 0;
	public int[] roundSeen = new int[MAX_UNITS];
	public double[] allies = new double[UNITS.length];
	public double[] enemies = new double[UNITS.length];
	
	
	public SpawnController(RobotState rs)
	{
		this.rs = rs;
	}
	
	public String toString()
	{
		String s = "";
		for (int i = 0; i < UNITS.length; i++)
		{
			s+= enemies[i] + " ";
		}
		s += build();
		return s;
	}
	
	public void sense()
	{
		for (int i = 0; i < UNITS.length; i++)
		{
			allies[i]*=DECAY;
			enemies[i]*=DECAY;
		}
		
		double old;
		int id;
		RobotType type;
		for (int i = 0; i < rs.info.allies.length; i++)
		{
			type = rs.info.allies[i].info.type;
			id = rs.info.allies[i].info.robot.getID();
			if (COUNTER[type.ordinal()] == null)
				continue;
			if (roundSeen[id] != 0)
				old = Math.pow(DECAY, rs.info.allies[i].round-roundSeen[id]);
			else old = 0;
			roundSeen[id%MAX_UNITS] = rs.info.allies[i].round;
			
			this.allies[type.ordinal()] += type.spawnCost*(1- old);
		}

		for (int i = 0; i < rs.info.enemies.length; i++)
		{
			type = rs.info.enemies[i].info.type;
			id = rs.info.enemies[i].info.robot.getID();
			if (COUNTER[type.ordinal()] == null)
				continue;
			if (roundSeen[id] != 0)
				old = Math.pow(DECAY, rs.info.enemies[i].round-roundSeen[id]);
			else old = 0;
			roundSeen[id%MAX_UNITS] = rs.info.enemies[i].round;
			
			this.enemies[type.ordinal()] += type.spawnCost*(1- old);
		}
		
	}
	
	public RobotType build()
	{
		double[] diff = new double[UNITS.length];
		double alliesTotal = 0.0;
		double enemiesTotal = 0.0;
		for (int i = 0; i < UNITS.length; i++)
		{
			alliesTotal += allies[i];
			enemiesTotal += enemies[i];
		}
		
		for (int i = 0; i < UNITS.length; i++)
		{
			for (int j = 0; j < UNITS.length; j++)
			{
				if (enemiesTotal == 0.0)
					diff[j] += COUNTER[i][j]*1.0/6; // = 1/4
				else diff[j] += COUNTER[i][j]*enemies[i]/enemiesTotal;
			}

			if (alliesTotal == 0.0)
				diff[i] -= 0.25; // = 1/4
			else diff[i] -= allies[i]/alliesTotal;
		}

		int min = 1;
		for (int i = 1; i < UNITS.length-1; i++)
		{
			if (diff[i] > diff[min])
				min = i;
		}
		return UNITS[min];
	}
	
	public void run() throws GameActionException
	{
		sense();
		RobotType next;
		if (rs.gstate == GameState.TOWERS)
			next = RobotType.SOLDIER;
		else next = build();
		boolean canSpawn = rs.rc.senseTerrainTile(rs.rc.getLocation().add(rs.rc.getDirection())) == TerrainTile.LAND
				&& rs.rc.senseObjectAtLocation(rs.rc.getLocation().add(rs.rc.getDirection()), next.level) == null;
		if (!canSpawn && rs.mstate != MoveState.FIGHTING)
		{
			rs.rc.setDirection(rs.rc.getDirection().rotateLeft());
			return;
		}

		double flux = next.spawnCost;
		if (rs.mstate == MoveState.FIGHTING)
			flux += FluxController.getFlux(next, FluxController.FIGHT_FLUX);
		else
			flux += FluxController.getFlux(next, FluxController.MOVE_FLUX);
		flux = Math.min(flux, rs.rc.getType().maxFlux);

		if (rs.rc.getFlux() >= flux && canSpawn)
		{
			rs.rc.spawn(next);
			fluxUsed += flux;
			if (fluxUsed > FLUX_CYCLE[fluxCounter])
			{
				fluxCounter++;
				done_spawning = true;
			}
		}
	}
}
