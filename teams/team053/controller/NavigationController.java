package team053.controller;
import team053.*;
import team053.state.*;
import battlecode.common.*;
public class NavigationController {
	public RobotState rs;
	public MoveController mc;
	public SpawnController sc;
	public TowerController tc;
	public FightMoveController fc;
	
	public static final double MIN_FLUX = 5.0;
	
	public NavigationController(RobotState rs)
	{
		this.rs = rs;
		mc = new MoveController(rs);
		sc = new SpawnController(rs);
		tc = new TowerController(rs);
		fc = new FightMoveController(rs);
	}
	
	public void run() throws GameActionException
	{ 
		if (rs.rc.getFlux() < rs.rc.getType().moveCost*MIN_FLUX && rs.rc.getType() != RobotType.ARCHON)
			return;
				
		if (rs.mstate != MoveState.IDLE && rs.mstate != MoveState.FIGHTING)
			mc.run();
		else if (rs.mstate == MoveState.FIGHTING)
		{
			if (rs.rc.getType() != RobotType.ARCHON)
				fc.run();
			else
			{
				sc.run();
				if (!rs.rc.isMovementActive())
					fc.run();
			}
		}
		else if (rs.rc.getType() == RobotType.ARCHON && rs.gstate == GameState.SPAWN)
			sc.run();
		else if (rs.rc.getType() == RobotType.ARCHON && rs.gstate == GameState.TOWERS)
		{
			tc.run();
		}
	}
}
