package team053.controller;
import team053.FastRobotInfo;
import team053.RobotState;
import team053.state.FightState;
import team053.state.GameState;
import battlecode.common.*;
public class AttackController 
{
	public RobotState rs;
	public RobotType type;
	public FastRobotInfo towerTarget;
	public FastRobotInfo target;

	public AttackController(RobotState rs)
	{
		this.rs = rs;
		this.type = rs.rc.getType();		
	}

	public void run() throws GameActionException
	{
		if (rs.fstate == FightState.KITE)
			attackNearest();
		else if (rs.fstate == FightState.DOGFIGHT)
			attackWeakest();
		else if (rs.fstate == FightState.PUSH)
			attackFarthest();
	}
	
	public FastRobotInfo[] getScorchers()
	{
		int enemyScorcherCount = 0;

		for (FastRobotInfo x: rs.info.enemies)
		{
			if (x.info.type == RobotType.SCORCHER && rs.rc.canAttackSquare(x.info.location))
				enemyScorcherCount++;
		}
		
		FastRobotInfo[] nearbyEnemyScorchers = new FastRobotInfo[enemyScorcherCount];
		int count = 0;
		for (FastRobotInfo y: rs.info.enemies)
		{
			if (y.info.type == RobotType.SCORCHER && rs.rc.canAttackSquare(y.info.location))
			{
				nearbyEnemyScorchers[count]=y;
				count++;
			}
		}
		
		return nearbyEnemyScorchers;
	}

	public void attackWeakest() throws GameActionException
	{
		target=null;
		towerTarget=null;
		FastRobotInfo[] nearbyEnemyRobots = rs.info.enemies;
		FastRobotInfo[] nearbyRobots = rs.info.nearby;
		FastRobotInfo[] nearbyEnemyScorchers = getScorchers();
		
		if (rs.rc.getType() == RobotType.SCORCHER)
		{
			int numGroundEnemies=0;
			int numGroundUnits=0;
			for (FastRobotInfo x: nearbyEnemyRobots)
			{
				if (x.info.type != RobotType.SCOUT && rs.rc.canAttackSquare(x.info.location))
					numGroundEnemies++;
			}
			for (FastRobotInfo x: nearbyRobots )
			{
				if (x.info.type != RobotType.SCOUT && rs.rc.canAttackSquare(x.info.location))
					numGroundUnits++;
			}
			if (numGroundEnemies*2>numGroundUnits)
				rs.rc.attackSquare(rs.rc.getLocation().add(rs.rc.getDirection()), RobotLevel.ON_GROUND);
			return;
		}
		//Attack weakest
		double currentLowestEnergon = 10000;
		
		if (nearbyEnemyScorchers.length>0)
		{
			for (FastRobotInfo other: nearbyEnemyScorchers)
			{
				if (other.info.energon<currentLowestEnergon)
				{
					currentLowestEnergon=other.info.energon;
					target=other;
				}
			}
		}
		else
		{
			for (FastRobotInfo other: nearbyEnemyRobots)
			{
				if (other.info.energon<currentLowestEnergon && rs.rc.canAttackSquare(other.info.location))
				{
					if ((other.info.type != RobotType.TOWER) || rs.gstate == GameState.TOWERS)
					{
						currentLowestEnergon=other.info.energon;
						target=other;
					}
					else
						towerTarget=other;
				}
			}
		}

		//Attack, if possible
		if (target!=null)
			rs.rc.attackSquare(target.info.location, target.r.getRobotLevel());
		else if (towerTarget!=null)
			rs.rc.attackSquare(towerTarget.info.location, towerTarget.r.getRobotLevel());
	}

	public void attackFarthest() throws GameActionException
	{
		target=null;
		towerTarget=null;
		FastRobotInfo[] nearbyEnemyRobots = rs.info.enemies;
		FastRobotInfo[] nearbyRobots = rs.info.nearby;
		FastRobotInfo[] nearbyEnemyScorchers = getScorchers();
		
		if (rs.rc.getType() == RobotType.SCORCHER)
		{
			int numGroundEnemies=0;
			int numGroundUnits=0;
			for (FastRobotInfo x: nearbyEnemyRobots)
			{
				if (x.info.type != RobotType.SCOUT && rs.rc.canAttackSquare(x.info.location))
					numGroundEnemies++;
			}
			for (FastRobotInfo x: nearbyRobots )
			{
				if (x.info.type != RobotType.SCOUT && rs.rc.canAttackSquare(x.info.location))
					numGroundUnits++;
			}
			if (numGroundEnemies*2>numGroundUnits)
				rs.rc.attackSquare(rs.rc.getLocation().add(rs.rc.getDirection()), RobotLevel.ON_GROUND);
			return;
		}
		//Attack Farthest

		int currentHighestDistance = -1;
		int highDistanceFrom=-1;

		if (nearbyEnemyScorchers.length>0)
		{
			for (FastRobotInfo other: nearbyEnemyScorchers)
			{
				highDistanceFrom = rs.rc.getLocation().distanceSquaredTo(other.info.location);
				if (highDistanceFrom>currentHighestDistance)
				{
					target=other;
					currentHighestDistance=highDistanceFrom;
				}
			}
		}
		else 
		{
			for (FastRobotInfo other: nearbyEnemyRobots)
			{
				highDistanceFrom = rs.rc.getLocation().distanceSquaredTo(other.info.location);

				if (highDistanceFrom>currentHighestDistance && rs.rc.canAttackSquare(other.info.location))
				{
					if (other.info.type != RobotType.TOWER)
					{
						currentHighestDistance=highDistanceFrom;
						target=other;
					}
					else
						towerTarget=other;
				}
			}
		}

		//Attack, if possible
		if (target!=null)
			rs.rc.attackSquare(target.info.location, target.r.getRobotLevel());
		else if (towerTarget!=null)
			rs.rc.attackSquare(towerTarget.info.location, towerTarget.r.getRobotLevel());
	}

	public void attackNearest() throws GameActionException
	{
		target=null;
		towerTarget=null;
		FastRobotInfo[] nearbyEnemyRobots = rs.info.enemies;
		FastRobotInfo[] nearbyRobots = rs.info.nearby;
		FastRobotInfo[] nearbyEnemyScorchers = getScorchers();
		
		if (rs.rc.getType() == RobotType.SCORCHER)
		{
			int numGroundEnemies=0;
			int numGroundUnits=0;
			for (FastRobotInfo x: nearbyEnemyRobots)
			{
				if (x.info.type != RobotType.SCOUT && rs.rc.canAttackSquare(x.info.location))
					numGroundEnemies++;
			}
			for (FastRobotInfo x: nearbyRobots )
			{
				if (x.info.type != RobotType.SCOUT && rs.rc.canAttackSquare(x.info.location))
					numGroundUnits++;
			}
			if (numGroundEnemies*2>numGroundUnits)
				rs.rc.attackSquare(rs.rc.getLocation().add(rs.rc.getDirection()), RobotLevel.ON_GROUND);
			return;
		}
		
		//Attack nearest
		int currentLowestDistance = 10000;
		int distanceFrom=10000;

		if (nearbyEnemyScorchers.length>0)
		{
			for (FastRobotInfo other: nearbyEnemyScorchers)
			{
				distanceFrom = rs.rc.getLocation().distanceSquaredTo(other.info.location);
				if (distanceFrom<currentLowestDistance)
				{
					target=other;
					currentLowestDistance=distanceFrom;
				}
			}
		}
		else 
		{
			for (FastRobotInfo other: nearbyEnemyRobots)
			{
				distanceFrom = rs.rc.getLocation().distanceSquaredTo(other.info.location);

				if (distanceFrom<currentLowestDistance && rs.rc.canAttackSquare(other.info.location))
				{
					if (other.info.type != RobotType.TOWER)
					{
						currentLowestDistance=distanceFrom;
						target=other;
					}
					else
						towerTarget=other;
				}
			}
		}

		//Attack, if possible
		if (target!=null)
			rs.rc.attackSquare(target.info.location, target.r.getRobotLevel());
		else if (towerTarget!=null)
			rs.rc.attackSquare(towerTarget.info.location, towerTarget.r.getRobotLevel());
	}

}
