package team053.controller;
import team053.RobotState;
import team053.state.FightState;
import team053.state.GameState;
import team053.state.MoveState;
import battlecode.common.*;
public class MoveController {
	public RobotState rs;
	public Bugger bugger;
	public MapLocation from;
	public MapLocation to;
	public Direction moveTo;
	public int delay = 0;
	
	public MapLocation[] retreat;
	public int rCount;
	
	public static final int PATIENCE = 5;

	public MoveController (RobotState rs)
	{
		this.rs = rs;
		bugger = new Bugger(rs);
		retreat = new MapLocation[10]; //10 should be enough
		rCount = 0;
	}

	public void startBug()
	{
		moveTo = null;
		bugger.from = this.from;
		bugger.startBug(to);
	}
	
	public void run() throws GameActionException 
	{
		if (rs.rc.canSenseSquare(to))
		{
			if (rs.rc.senseTerrainTile(to) == TerrainTile.VOID)
				moveTo = Direction.OMNI;
			else if (rs.rc.getType() == RobotType.ARCHON)
			{
				if (rs.rc.senseObjectAtLocation(to, rs.rc.getType().level) != null
						&& to.distanceSquaredTo(rs.rc.getLocation()) <= 2)
				{
					moveTo = Direction.OMNI;
				}
			}
		}
		if (rs.rc.getType() != RobotType.ARCHON)
		{
			if (rs.mem.nodeLoc.contains(to) &&
					to.distanceSquaredTo(rs.rc.getLocation()) <= rs.rc.getType().attackRadiusMaxSquared)
			{
				if (to.equals(rs.rc.getLocation()))
				{
					Direction dir = rs.rc.getDirection();
					for (int i = 0; i < 8 && !rs.rc.canMove(dir); i++)
					{
						dir = dir.rotateLeft();
					}
					moveTo = dir;
				}
				else moveTo = Direction.OMNI;
			}
		}
		
		if (moveTo == null)
			moveTo = bugger.bug();
		if (moveTo == Direction.OMNI)
		{
			if (rs.mstate == MoveState.PATROL)
			{
				MapLocation temp = from;
				from = to;
				to = temp;
				startBug();
			}
			else
			{
				rs.mstate = MoveState.IDLE;
			}
			
			if (rs.rc.getType() == RobotType.ARCHON && rs.mem.nodeLoc.contains(to))
			{
				rs.mem.expandLoc.add(to);
			}
		}
		else if (moveTo == Direction.NONE)
		{
			moveTo = null;
		}
		else{
			if (rs.mstate == MoveState.RETREATING)
			{
				if (rs.rc.getDirection().opposite() == moveTo)
				{
					if (rs.rc.canMove(moveTo))
					{
						rs.rc.moveBackward();
						moveTo = null;
						delay = 0;
					}
					else if (delay == PATIENCE)
					{
						delay = 0;
						moveTo = null;
					}
					else delay++;
				}
				else
				{
					rs.rc.setDirection(moveTo.opposite());
				}
			}
			else
			{
				if (rs.rc.getDirection() == moveTo)
				{
					if (rs.rc.canMove(moveTo))
					{
						rs.rc.moveForward();
						moveTo = null;
						delay = 0;
					}
					else if (delay == PATIENCE)
					{
						delay = 0;
						moveTo = null;
					}
					else delay++;
				}
				else
				{
					rs.rc.setDirection(moveTo);
				}
			}
		}
	}
}
