package team053.controller;

import team053.RobotState;

import team053.state.GameState;
import team053.state.MoveState;
import battlecode.common.*;
public class TowerController {
	public RobotState rs;
	
	public TowerController(RobotState rs)
	{
		this.rs = rs;
	}
	public void run() throws GameActionException
	{
		if (rs.rc.senseObjectAtLocation(rs.rc.getLocation(), RobotLevel.POWER_NODE) != null)
		{
			if (rs.rc.canMove(rs.rc.getDirection().opposite()))
				rs.rc.moveBackward();
			else rs.rc.setDirection(rs.rc.getDirection().rotateLeft());
		}
		else if(rs.rc.senseObjectAtLocation(rs.rc.getLocation().add(rs.rc.getDirection()), RobotLevel.POWER_NODE) != null)
		{
			GameObject o = rs.rc.senseObjectAtLocation(rs.rc.getLocation().add(rs.rc.getDirection()), RobotLevel.ON_GROUND);
			if (o == null)
			{
				if (rs.rc.getFlux() >= RobotType.TOWER.spawnCost)
					rs.rc.spawn(RobotType.TOWER);
			}
			else if (o.getTeam() == rs.rc.getTeam())
			{
				if (o instanceof Robot)
				{
					RobotInfo ri= rs.rc.senseRobotInfo((Robot)o);
					if (ri.type != RobotType.ARCHON)
						towers();
				}
			}
			else rs.nc.sc.run();
		}
		else if (rs.mstate == MoveState.IDLE)
		{
			if (rs.info.enemies.length > 0)
			{
				rs.nc.sc.run();
			}
			else towers();
		}
		else {
			for (int i = 0; i < rs.info.archons.length; i++)
			{
				if (rs.info.archons[i].equals(rs.nc.mc.to))
					towersSafe();
			}
		}
	}
	
	public void towers()
	{
		//closest capturable point
		int closest = 0;
		int distance = 10000;
		for (int i = 0; i < rs.info.capturable.length; i++)
		{
			int d = rs.rc.getLocation().distanceSquaredTo(rs.info.capturable[i]);
			if (d < distance)
			{
				closest = i;
				distance= d; 
			}
		}

		rs.nc.mc.to = rs.info.capturable[closest];
		rs.nc.mc.from = rs.rc.getLocation();
		rs.nc.mc.startBug();
		rs.mstate = MoveState.BUGGING;
	}
	
	public void towersSafe()
	{
		//safest capturable point (closest to base)
		int closest = 0;
		int distance = 10000;
		for (int i = 0; i < rs.info.capturable.length; i++)
		{
			int d = rs.mem.core.getLocation().distanceSquaredTo(rs.info.capturable[i]);
			if (d < distance)
			{
				closest = i;
				distance= d; 
			}
		}

		rs.nc.mc.to = rs.info.capturable[closest];
		rs.nc.mc.from = rs.rc.getLocation();
		rs.nc.mc.startBug();
		rs.mstate = MoveState.BUGGING;
	}
}
