package team053.controller;
import team053.FastRobotInfo;
import team053.RobotState;
import team053.state.MoveState;
import battlecode.common.*;
public class FluxController {
	public RobotState rs;
	public static final double AVG_MAP_SIZE = (GameConstants.MAP_MAX_HEIGHT+GameConstants.MAP_MIN_HEIGHT)/2.0;
	public RobotType type;
	public static final double MIN_FLUX = 20.0;
	public static final double RETREAT_FLUX = 20.0;
	public static final double MOVE_FLUX = 1.0;
	public static final double FIGHT_FLUX = 0.2;
	public static final double FLUX_UPKEEP = 50.0; //=500 rounds
	
	public FluxController(RobotState rs)
	{
		this.rs = rs;
		type = rs.rc.getType();
	}
	
	public static double getFlux(RobotType type, double fluxConstant)
	{
		return Math.min(AVG_MAP_SIZE*fluxConstant*type.moveCost+FLUX_UPKEEP*GameConstants.UNIT_UPKEEP,
				type.maxFlux);
	}
	
	public void run() throws GameActionException
	{
		//transfered flux scaled by energon: getFlux/theirFlux = getEnergon()/TheirEnergon 
		//for non-archon units: if attack active && movement active, turn around and transfer flux next turn to units who need it
		//for archon units: transfer constant flux to non-archon units determined by AVG_MAP_SIZE*2*move_cost
		FastRobotInfo[] r = rs.info.nearby;
		for( int k = 0; k < r.length; k++)
		{
			double a = rs.rc.getFlux();
			double b = r[k].info.flux;
			double e = rs.rc.getEnergon();
			double f = r[k].info.energon;
			if (r[k].info.type.moveCost <= 0 || r[k].info.team != rs.rc.getTeam()
					|| !rs.rc.getLocation().isAdjacentTo(r[k].info.location))
				continue;
			
			if(type == RobotType.ARCHON)
			{
				if (rs.mstate == MoveState.FIGHTING)
				{
					double flux = getFlux(r[k].info.type, FIGHT_FLUX);
					if (rs.rc.getFlux() >= flux && r[k].info.flux <= r[k].info.type.moveCost*MIN_FLUX)
					{
						rs.rc.transferFlux(r[k].info.location,r[k].r.getRobotLevel(), flux);
					}
				}
				else
				{
					double flux = getFlux(r[k].info.type, MOVE_FLUX);
					if (rs.rc.getFlux() >= flux && r[k].info.flux <= r[k].info.type.moveCost*MIN_FLUX)
					{
						rs.rc.transferFlux(r[k].info.location,r[k].r.getRobotLevel(), flux);
					}
				}
			}
			else if (r[k].info.flux > GameConstants.UNIT_UPKEEP*MIN_FLUX
					&& rs.rc.isAttackActive() //&& rs.rc.isMovementActive()
					&& a*f > b*e) 
			{
				rs.rc.transferFlux(r[k].info.location, r[k].r.getRobotLevel(), (a*f - b*e)/(e+f));
				break;
			}
		}
	}
}
