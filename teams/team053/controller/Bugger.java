package team053.controller;

import team053.*;
import team053.utility.*;
import battlecode.common.*;

public class Bugger {
	public RobotState rs;
	public boolean hugLeft;
	public MapLocation from;
	public MapLocation to;
	public boolean hugging;
	public Direction hugDir;
	public FastLocDirSet seen;
	public boolean recursed;
	
	public static final Direction[][] DIR = new Direction[][]{
		new Direction[]{Direction.NORTH_WEST, Direction.NORTH, Direction.NORTH_EAST},
		new Direction[]{Direction.WEST, Direction.OMNI, Direction.EAST},
		new Direction[]{Direction.SOUTH_WEST,Direction.SOUTH,Direction.SOUTH_EAST}
	};
	
	public Bugger(RobotState rs)
	{
		this.rs = rs;
	}
	
	public void startBug(MapLocation loc)
	{
		from = rs.rc.getLocation();
		to = loc;
		hugging = false;
		hugLeft = true;
		seen = new FastLocDirSet();
	}
	
	public Direction bug()
	{
		Direction desiredDir = Bugger.direction(rs.rc.getLocation(),to);
		if (desiredDir == Direction.NONE || desiredDir == Direction.OMNI)
			return desiredDir;
		if (hugging)
		{
			Direction bestDir = goInDir(desiredDir);
			if (seen.contains(rs.rc.getLocation(), rs.rc.getDirection()))
			{
				hugging = false;
			}
			
			if (bestDir != null)
			{
				if (rs.rc.canMove(bestDir) && rs.rc.getLocation().distanceSquaredTo(to) <= from.distanceSquaredTo(to))
				{
					hugging = false;
				} 
			}
			seen.add(rs.rc.getLocation(), rs.rc.getDirection());
		}
		
		if (!hugging)
		{
			Direction bestDir = goInDir(desiredDir);
			if (bestDir != null)
	            return bestDir;
			
			seen.clear();
			hugging = true;
			from = rs.rc.getLocation();
			hugDir = desiredDir;
			recursed = false;
			return hug();
		}
		else
		{
			return hug();
		}
	}
	
	private Direction turn(Direction dir){
	    return (hugLeft ? dir.rotateLeft() : dir.rotateRight());
	}
	
	private Direction oppositeTurn(Direction dir)
	{
		return (hugLeft ? dir.rotateRight() : dir.rotateLeft());
	}

	private Direction hug (){
		MapLocation loc = rs.rc.getLocation();
	    Direction tryDir = hugDir;
	    MapLocation tryLoc = loc.add(tryDir);
	    int i = 0;
	    for (i = 0; i < 8 && !rs.rc.canMove(tryDir); i++){
	    	if (rs.rc.canSenseSquare(tryLoc) && rs.rc.senseTerrainTile(tryLoc) == TerrainTile.OFF_MAP)
	    		break;
	        tryDir = turn(tryDir);
	        tryLoc = loc.add(tryDir);
	    }
	    
	    if (rs.rc.canMove(tryDir))
	    {
			hugDir = tryDir;
	    	if (hugDir.isDiagonal())
	    		hugDir = oppositeTurn(hugDir);
    		hugDir = oppositeTurn(hugDir);
    		hugDir = oppositeTurn(hugDir);
	    	return tryDir;
	    }
	    else if (i == 8)
	    {
	    	//blocked in all directions
	    	return Direction.NONE;
	    }
	    else if (!recursed)
	    {
	    	seen.clear();
			hugDir = oppositeTurn(hugDir);
	    	hugLeft = !hugLeft;
	    	recursed = true;
	    	return hug();
	    }
	    else
	    {
	    	//something went wrong, reset
	    	startBug(to);
	    	return Direction.NONE;
	    }
	}
	
	public Direction goInDir(Direction desiredDir){
	    Direction[] directions = new Direction[3];
	    directions[0] = desiredDir;
	    Direction left = desiredDir.rotateLeft();
	    Direction right = desiredDir.rotateRight();
	    boolean leftIsBetter = (rs.rc.getLocation().add(left).distanceSquaredTo(to) < 
	    		rs.rc.getLocation().add(right).distanceSquaredTo(to));
	    directions[1] = (leftIsBetter ? left : right);
	    directions[2] = (leftIsBetter ? right : left);


	    for (int i = 0; i < directions.length; i++){
	        if (rs.rc.canMove(directions[i])){
	            return directions[i];
	        }
	    }
	    return null;
	}
	
	public static Direction direction(MapLocation a, MapLocation b)
	{
		int x = b.x-a.x;
		int y = b.y-a.y;
		if (x < 0) x=0;
		else if (x==0) x=1;
		else x=2;
		
		if (y < 0) y=0;
		else if (y==0) y=1;
		else y=2;
		
		return DIR[y][x];
	}
}
