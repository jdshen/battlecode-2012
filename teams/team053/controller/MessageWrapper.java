package team053.controller;

import team053.state.*;
import battlecode.common.*;

public class MessageWrapper {
	public MessageState state;
	public MapLocation loc;
	public int roundNum;
	public boolean corrupted;
	
	public static final int PASS = 48277;
	public static final int CHECK_HASH = 6599;
	public static final int OLD = 6;
	public static final int LENGTH = 6;
	
	public MessageWrapper()
	{	
	}
	
	public Message toMessage()
	{
		String s = "";
		int checkSum = (PASS%CHECK_HASH + roundNum%CHECK_HASH + state.ordinal()%CHECK_HASH)%CHECK_HASH;
		checkSum = (checkSum + loc.x + loc.y)%CHECK_HASH;
		
		//loc must not be null, even if loc is not required
		s += (char)PASS;
		s += (char)roundNum;
		s += (char)checkSum;
		s += (char)state.ordinal();
		s += ((char)loc.x);
		s += ((char)loc.y);
				
		Message m = new Message();
		m.strings = new String[]{s};
		return m;
	}
	
	public static MessageWrapper getWrapper(Message m, int roundNum)
	{
		MessageWrapper mw = new MessageWrapper();
		mw.corrupted = false;
		if (m.strings == null || m.strings.length != 1 || m.strings[0].length() != LENGTH)
		{
			mw.corrupted = true;
			return mw;
		}
		String s = m.strings[0];
		int checkSum = 0;
		if ((int)s.charAt(0) != PASS ||(int)s.charAt(1) <= roundNum-OLD)
		{
			mw.corrupted = true;
			return mw;
		}
		else
		{
			checkSum = (checkSum+(int)s.charAt(0) + (int)s.charAt(1) + (int)s.charAt(3))%CHECK_HASH;
			checkSum = (checkSum + (int)s.charAt(4) + (int)s.charAt(5))%CHECK_HASH;
			if (checkSum != (int)s.charAt(2))
			{
				mw.corrupted = true;
				return mw;
			}
			mw.loc = new MapLocation((int)s.charAt(4), (int)s.charAt(5));
			mw.state = MessageState.values()[(int)s.charAt(3)];
			mw.roundNum = (int)s.charAt(1);
		}
		return mw;
	}
}
