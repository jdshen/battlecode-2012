package team053.controller;

import team053.FastRobotInfo;
import team053.RobotState;
import battlecode.common.GameActionException;

public class RegenController 
{
	public static final int MIN_DAMAGED_NEARBY = 2; //min number of units damaged nearby for regen
	public RobotState rs;
	
	public RegenController(RobotState rs)
	{
		this.rs = rs;
	}
	
	public void run() throws GameActionException
	{
		FastRobotInfo[] nearby = rs.info.nearby;
		
		int numDamagedNearby=0;
		
		for (FastRobotInfo robot: nearby)
		{
			if (robot.info.energon<robot.info.type.maxEnergon && robot.info.team==rs.rc.getTeam() 
					&& rs.rc.canAttackSquare(robot.info.location)
					&& !robot.info.regen)
			{
				numDamagedNearby++;
			}
		}
		
		if (numDamagedNearby>= MIN_DAMAGED_NEARBY)
		{
			rs.rc.regenerate();
		}
	}			
}