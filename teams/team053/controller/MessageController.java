package team053.controller;
import team053.*;
import team053.controller.*;
import team053.state.*;
import team053.utility.*;
import battlecode.common.*;

public class MessageController {
	public RobotState rs;
	public static final int[][] SPLIT_DIR = new int[][]{
		new int[]{-100,-100}, new int[]{100,100}, new int[]{100,-100}, new int[]{-100,100},
		new int[]{-100, 0}, new int[]{0,-100}, new int[]{0,100}, new int[]{100, 0},
		new int[]{0, 0}
	};
	
	public boolean clearMessages;
	public MessageWrapper[] mw;
	public int count;
	public int delay = 0;
	private int[] fightMessages = new int[BROADCAST_DELAY]; //recent messages about enemies
	private int fCount = 0;
	public int numAllied = 0;
	public static final int BROADCAST_DELAY = 5;
	public static final int MESSAGE_LIMIT = 5; //#messages to receive per turn
	public static final double MIN_FLUX = 2.0;
	public static final int DOGFIGHT_THRESHOLD = 5;
	public static final int PUSHING_THRESHOLD = 10;
	public MessageWrapper rebroadcast;
	
	public MessageController(RobotState rs)
	{
		this.rs = rs;
		mw = new MessageWrapper[MESSAGE_LIMIT];
		//throw out all messages when created
		clearMessages = false;
		if (rs.rc.getType() != RobotType.ARCHON)
		{
			clearMessages = true;
		}
	}
	
	public void run() throws GameActionException
	{
		//robot that is just turned back on disregards all its messages
		if (rs.rc.getFlux() <= GameConstants.UNIT_UPKEEP*MIN_FLUX)
		{
			clearMessages = true;
			return;
		}
		if (clearMessages)
		{
			rs.rc.getAllMessages();
			clearMessages = false;
			return;
		}
		
		getMessages();
		//always check for enemy units, overrides all other states
		fight();

		if (rs.rc.hasBroadcasted() || rs.mstate == MoveState.FIGHTING || rs.mstate == MoveState.PUSHING)
			return;
		else if (rs.gstate == GameState.SPLIT && Clock.getRoundNum() == 0)
		{
			split();
		}
		else if (rs.gstate == GameState.SPAWN)
		{
			if (rs.rc.getType() != RobotType.ARCHON && rs.mstate == MoveState.IDLE)
			{
				receiveExpand();
			}
			else if (rs.rc.getType() == RobotType.ARCHON && rs.mstate == MoveState.IDLE)
			{
				sendSpawning();
			}
		}
		else if (rs.gstate == GameState.EXPAND)
		{
			if(rs.rc.getType() == RobotType.ARCHON)
			{
				if (rs.mstate == MoveState.IDLE)
					sendExpand();
//				else receiveExpand();
			}
			else if (rs.rc.getType() != RobotType.ARCHON)
			{
				if (rs.mstate == MoveState.IDLE)
					sendExpand();
				else receiveExpand();
			}
		}
		else if (rs.gstate == GameState.DEFEND)
		{
			if(rs.rc.getType() == RobotType.ARCHON)
			{
				if (rs.mstate == MoveState.IDLE)
					sendDefend();
				else receiveSpawning();
			}
			else if (rs.rc.getType() != RobotType.ARCHON)
			{
				if (rs.mstate == MoveState.IDLE)
					sendExpand();
				else receiveExpand();
			}
		}
		else if (rs.gstate == GameState.TOWERS)
		{
			if(rs.rc.getType() == RobotType.ARCHON)
			{
//				sendTowerRally();
			}
			else if (rs.rc.getType() != RobotType.ARCHON)
			{
				if (rs.mstate == MoveState.IDLE)
					sendTowerRally();
				else receiveTowerRally();
			}
		}
	}
	
	public void getMessages()
	{
		int fMessages = 0;
		Message m;
		count = 0;
		while (count < MESSAGE_LIMIT && (m=rs.rc.getNextMessage()) != null)
		{
			mw[count] = MessageWrapper.getWrapper(m, Clock.getRoundNum());
			if (!mw[count].corrupted)
			{
				if (mw[count].state == MessageState.ENEMY)
				{
					fMessages++;
				}
				count++;
			}
		}

		numAllied += fMessages - fightMessages[fCount];
		fightMessages[fCount] = fMessages;
		fCount = (fCount+1)%fightMessages.length;
		if (rs.mstate == MoveState.FIGHTING)
		{
			if (rs.mc.numAllied >= PUSHING_THRESHOLD)
				rs.fstate = FightState.PUSH;
			else if (rs.mc.numAllied >= DOGFIGHT_THRESHOLD)
				rs.fstate = FightState.DOGFIGHT;
			else rs.fstate = FightState.KITE;
			if (rs.rc.getType() == RobotType.SCORCHER)
				rs.fstate = FightState.PUSH;
		}
	}
	
	public void fight() throws GameActionException
	{
		if (rs.mstate != MoveState.FIGHTING)
		{
			for (int i = 0; i < count; i++)
			{
				if (mw[i].state == MessageState.ENEMY)
				{
					if (rs.mstate != MoveState.PUSHING && rs.mstate != MoveState.RETREATING)
					{
						if (rs.rc.getType() != RobotType.ARCHON)
							rs.gstate = GameState.EXPAND;
						rs.mstate = MoveState.PUSHING;
						rs.nc.mc.from = rs.rc.getLocation();
						rs.nc.mc.to = mw[i].loc;
						rs.nc.mc.startBug();
					}
					if (rs.rc.getType() == RobotType.ARCHON && rs.gstate == GameState.TOWERS)
						rs.gstate = GameState.SPAWN;
					rs.sc.lastEnemy = 0;
					//rebroadcast
					if (delay == 0)
					{
						broadcast(mw[i].toMessage());
						delay = BROADCAST_DELAY;
					}
					else
					{
						rebroadcast = mw[i];
						delay--;
					}
					return;
				}
			}
		}
		
		if (delay == 0)
		{
			boolean nearbyEnemies = false;
			for (int i = 0; i < rs.info.enemies.length; i++)
			{
				if (rs.info.enemies[i].info.type != RobotType.TOWER
						&& rs.info.enemies[i].info.flux > GameConstants.UNIT_UPKEEP)
					nearbyEnemies =true;
			}
			if (nearbyEnemies)
			{
				MessageWrapper mw = new MessageWrapper();
				mw.state = MessageState.ENEMY;
				mw.loc = rs.info.enemies[0].info.location;
				mw.roundNum = Clock.getRoundNum();
				rs.nc.mc.to = rs.info.enemies[0].info.location;
				broadcast(mw.toMessage());
				delay = BROADCAST_DELAY;
				return;
			}
			

			if (rebroadcast != null)
			{
				broadcast(rebroadcast.toMessage());
				rebroadcast = null;
			}
		}
		else delay--;
		
	}
	
	public void sendSpawning() throws GameActionException
	{
		MessageWrapper send = new MessageWrapper();
		send.corrupted = false;
		send.roundNum = Clock.getRoundNum();
		send.state = MessageState.SPAWN;
		send.loc = rs.rc.getLocation();
		if (delay == 0)
		{
			broadcast(send.toMessage());
			delay = BROADCAST_DELAY;
		}
		else delay--;
	}
	
	public void sendTowerRally() throws GameActionException
	{
		MessageWrapper send = new MessageWrapper();
		send.corrupted = false;
		send.roundNum = Clock.getRoundNum();
		send.state = MessageState.RALLY;
		//closest capturable point
		int closest = 0;
		int distance = 10000;
		for (int i = 0; i < rs.info.capturable.length; i++)
		{
			int d = rs.rc.getLocation().distanceSquaredTo(rs.info.capturable[i]);
			if (d < distance)
			{
				closest = i;
				distance= d; 
			}
		}

		send.loc = rs.info.capturable[closest];
		broadcast(send.toMessage());
		rs.nc.mc.to = rs.info.capturable[closest];
		rs.nc.mc.from = rs.rc.getLocation();
		rs.nc.mc.startBug();
		rs.mstate = MoveState.BUGGING;
	}
	
	public void sendRally() throws GameActionException
	{
		MessageWrapper send = new MessageWrapper();
		send.corrupted = false;
		send.roundNum = Clock.getRoundNum();
		send.state = MessageState.RALLY;
		
		//closest neighbor
		for (int i = 0; i < rs.info.nearbyNodes.length; i++)
		{
			MapLocation[] neighbors = rs.info.nearbyNodes[i].neighbors();
			for (int j = 0; j < neighbors.length; j++)
			{
				if (!rs.mem.nodeLoc.contains(neighbors[j]))
				{
					//start bugging
					send.loc = neighbors[j];
					broadcast(send.toMessage());
					rs.nc.mc.to = neighbors[j];
					rs.nc.mc.from = rs.rc.getLocation();
					rs.nc.mc.startBug();
					rs.mstate = MoveState.BUGGING;
					return; //done for now
				}
			}
		}

		int closest = 0;
		int distance = 10000;
		for (int i = 0; i < rs.info.capturable.length; i++)
		{
			int d = rs.rc.getLocation().distanceSquaredTo(rs.info.capturable[i]);
			if (d < distance)
			{
				closest = i;
				distance= d; 
			}
		}

		send.loc = rs.info.capturable[closest];
		broadcast(send.toMessage());
		rs.nc.mc.to = rs.info.capturable[closest];
		rs.nc.mc.from = rs.rc.getLocation();
		rs.nc.mc.startBug();
		rs.mstate = MoveState.BUGGING;
	}
	
	public void sendDefend() throws GameActionException
	{
		rs.mem.expandLoc.clear();
		
		MessageWrapper send = new MessageWrapper();
		send.corrupted = false;
		send.roundNum = Clock.getRoundNum();
		send.state = MessageState.DEFEND;
		
		//closest capturable point
		int closest = 0;
		int distance = 10000;
		for (int i = 0; i < rs.info.alliedNodes.length; i++)
		{
			int d = rs.rc.getLocation().distanceSquaredTo(rs.info.alliedNodes[i].getLocation());
			if (d < distance)
			{
				closest = i;
				distance= d; 
			}
		}

		send.loc = rs.info.alliedNodes[closest].getLocation();
		broadcast(send.toMessage());
		rs.nc.mc.to = rs.info.alliedNodes[closest].getLocation();
		rs.nc.mc.from = rs.rc.getLocation();
		rs.nc.mc.startBug();
		rs.mstate = MoveState.BUGGING;
	}
	
	public void sendExpand() throws GameActionException
	{
		MessageWrapper send = new MessageWrapper();
		send.corrupted = false;
		send.roundNum = Clock.getRoundNum();
		send.state = MessageState.EXPAND;
		
		//closest neighbor
		
		if (rs.info.nearbyNodes.length > 0)
		{
			int min = 0;
			int minD = 10000;
			for (int i = 0; i < rs.info.nearbyNodes.length; i++)
			{
				int d = rs.rc.getLocation().distanceSquaredTo(rs.info.nearbyNodes[i].getLocation());
				if (d < minD)
				{
					minD = d;
					min = i;
				}
			}
			
//			MapLocation[] neighbors = rs.info.nearbyNodes[min].neighbors();
//			for (int j = 0; j < neighbors.length; j++)
//			{
//				int j2 = (j+rs.rc.getRobot().getID())%neighbors.length;
//				if (!rs.mem.expandLoc.contains(neighbors[j2]))
//				{
//					//start bugging
//					send.loc = neighbors[j2];
//					broadcast(send.toMessage());
//					rs.nc.mc.to = neighbors[j2];
//					rs.nc.mc.from = rs.rc.getLocation();
//					rs.nc.mc.startBug();
//					rs.mstate = MoveState.BUGGING;
//					rs.mem.nodeLoc.add(neighbors[j2]);
//					return; //done for now
//				}
//			}
			
			MapLocation[] neighbors = rs.info.nearbyNodes[min].neighbors();
			int max = 0;
			int maxD = 0;
			for (int j = 0; j < neighbors.length; j++)
			{
				if (!rs.mem.expandLoc.contains(neighbors[j]))
				{
					int d = neighbors[j].distanceSquaredTo(rs.mem.core.getLocation());
					if (d > maxD)
					{
						max = j;
						maxD = d;
					}
				}
			}
			
			if (maxD != 0)
			{
				//start bugging
				send.loc = neighbors[max];
				broadcast(send.toMessage());
				rs.nc.mc.to = neighbors[max];
				rs.nc.mc.from = rs.rc.getLocation();
				rs.nc.mc.startBug();
				rs.mstate = MoveState.BUGGING;
				rs.mem.nodeLoc.add(neighbors[max]);
				return; //done for now
			}
		}
	
		rs.mem.expandLoc.clear();
		//closest capturable point
		int closest = 0;
		int distance = 10000;
		for (int i = 0; i < rs.info.capturable.length; i++)
		{
			int d = rs.rc.getLocation().distanceSquaredTo(rs.info.capturable[i]);
			if (d < distance)
			{
				closest = i;
				distance= d; 
			}
		}
	
		send.loc = rs.info.capturable[closest];
		broadcast(send.toMessage());
		rs.nc.mc.to = rs.info.capturable[closest];
		rs.nc.mc.from = rs.rc.getLocation();
		rs.nc.mc.startBug();
		rs.mstate = MoveState.BUGGING;
	}

	public void receiveDefend()
	{
		//non archon units go into expand
		for (int i = 0; i < count; i++)
		{
			if (mw[i].state == MessageState.DEFEND)
			{
				rs.nc.mc.to = mw[i].loc;
				rs.nc.mc.from = rs.rc.getLocation();
				rs.nc.mc.startBug();
				rs.mstate = MoveState.BUGGING;
				rs.gstate = GameState.DEFEND;
				rs.mem.expandLoc.add(mw[i].loc);
				return; //done for now
			}
		}
	}
	
	public void receiveExpand()
	{
		//non archon units go into expand
		for (int i = 0; i < count; i++)
		{
			if (mw[i].state == MessageState.EXPAND)
			{
				rs.nc.mc.to = mw[i].loc;
				rs.nc.mc.from = rs.rc.getLocation();
				rs.nc.mc.startBug();
				rs.mstate = MoveState.BUGGING;
				rs.gstate = GameState.EXPAND;
				rs.mem.nodeLoc.add(mw[i].loc);
				return; //done for now
			}
		}
	}

	public void receiveRally()
	{
		//non archon units rally towards a point
		for (int i = 0; i < count; i++)
		{
			if (mw[i].state == MessageState.RALLY)
			{
				rs.nc.mc.to = mw[i].loc;
				rs.nc.mc.from = rs.rc.getLocation();
				rs.nc.mc.startBug();
				rs.mstate = MoveState.BUGGING;
				return; //done for now
			}
		}
	}
	
	public void receiveTowerRally()
	{
		//non archon units rally towards a point
		for (int i = 0; i < count; i++)
		{
			if (mw[i].state == MessageState.RALLY)
			{
				rs.nc.mc.to = mw[i].loc;
				rs.nc.mc.from = rs.rc.getLocation();
				rs.nc.mc.startBug();
				rs.mstate = MoveState.BUGGING;
				rs.mem.nodeLoc.add(mw[i].loc);
				return; //done for now
			}
		}
	}

	public void receiveSpawning()
	{
		//if other archon got to spawn point before this one, stop and start spawning again
		for (int i = 0; i < count; i++)
		{
			if (mw[i].state == MessageState.SPAWN && mw[i].loc.equals(rs.nc.mc.to))
			{
				rs.mstate = MoveState.IDLE;
				return; //done for now
			}
		}
	}

	public void split() throws GameActionException
	{
		FastLocSet set = new FastLocSet();
		for (int i = 0; i < count; i++)
		{
			if (mw[i].state == MessageState.SPLIT)
			{
				set.add(mw[i].loc);
			}
		}
		
		for (int i = 0; i < rs.info.capturable.length; i++)
		{
			if (!set.contains(rs.info.capturable[i]))
			{
				//send message
				MessageWrapper send = new MessageWrapper();
				send.corrupted = false;
				send.roundNum = Clock.getRoundNum();
				send.state = MessageState.SPLIT;
				send.loc = rs.info.capturable[i];
				//start bugging
				broadcast(send.toMessage());
				rs.nc.mc.to = rs.info.capturable[i];
				rs.nc.mc.from = rs.rc.getLocation();
				rs.nc.mc.startBug();
				rs.mstate = MoveState.BUGGING;
				rs.mem.expandLoc.add(rs.info.capturable[i]);
				return; //done for now
			}
		}
		
		for (int i = 0; i < SPLIT_DIR.length; i++)
		{
			MapLocation to = rs.mem.core.getLocation().add(SPLIT_DIR[i][0], SPLIT_DIR[i][1]);
			if (!set.contains(to))
			{
				//send message
				MessageWrapper send = new MessageWrapper();
				send.corrupted = false;
				send.roundNum = Clock.getRoundNum();
				send.state = MessageState.SPLIT;
				send.loc = to;
				//start bugging
				broadcast(send.toMessage());
				rs.nc.mc.to = to;
				rs.nc.mc.from = rs.rc.getLocation();
				rs.nc.mc.startBug();
				rs.mstate = MoveState.BUGGING;
				return; //done for now
			}
		}
	}
	
	public void broadcast(Message m) throws GameActionException
	{
		if (rs.rc.getFlux() >= m.getFluxCost())
			rs.rc.broadcast(m);
	}
}
