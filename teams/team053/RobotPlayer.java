package team053;

import team053.state.MoveState;
import battlecode.common.*;

public class RobotPlayer {

	public static void run(RobotController rc) {
		
		RobotState rs = new RobotState(rc);
		while (true) {
			try {
				rs.rc.setIndicatorString(0, rs.gstate+" " +rs.mstate + " "+rs.fstate);
				if (rs.nc.mc.to != null)
					rs.rc.setIndicatorString(1, rs.nc.mc.to.toString());
				else rs.rc.setIndicatorString(1, null);
//				rs.rc.setIndicatorString(2, rs.sc.lastEnemy+"");
//				rs.rc.setIndicatorString(2, rs.mc.numAllied+"");
				rs.rc.setIndicatorString(2, rs.nc.sc.toString());
				
				//sense
				Robot[] bots = rc.senseNearbyGameObjects(Robot.class);
				FastRobotInfo[] fri = new FastRobotInfo[bots.length];
				int alliedCount = 0;
				int enemyCount = 0;
				for (int i = 0; i < bots.length; i++)
				{
					fri[i] = new FastRobotInfo(bots[i], rc.senseRobotInfo(bots[i]), Clock.getRoundNum());
					if (fri[i].r.getTeam() == rs.rc.getTeam())
						alliedCount++;
					else enemyCount++;
				}
				
				FastRobotInfo[] allies = new FastRobotInfo[alliedCount];
				FastRobotInfo[] enemies = new FastRobotInfo[enemyCount];
				alliedCount = 0; enemyCount = 0;

				for (int i = 0; i < fri.length; i++)
				{
					if (fri[i].r.getTeam() == rs.rc.getTeam())
						allies[alliedCount++] = fri[i];
					else enemies[enemyCount++] = fri[i];
				}

				rs.info.nearby = fri;
				rs.info.allies = allies;
				rs.info.enemies = enemies;
				rs.info.alliedNodes = rc.senseAlliedPowerNodes();
				rs.info.archons = rc.senseAlliedArchons();
				rs.info.capturable = rc.senseCapturablePowerNodes();
				rs.info.nearbyNodes = rc.senseNearbyGameObjects(PowerNode.class);
				//add sensed info to mem info
				for (int i = 0; i < rs.info.alliedNodes.length; i++)
					rs.mem.nodeLoc.add(rs.info.alliedNodes[i].getLocation());
				for (int i = 0; i < rs.info.capturable.length; i++)
					rs.mem.nodeLoc.add(rs.info.capturable[i]);
				for (int i = 0; i < rs.info.nearbyNodes.length; i++)
					rs.mem.nodeLoc.add(rs.info.nearbyNodes[i].getLocation());
				for (int i = 0; i < rs.info.alliedNodes.length; i++)
					rs.mem.expandLoc.add(rs.info.alliedNodes[i].getLocation());

				//determine state from sensed info
				rs.sc.run();
				//read messages, determine state, send messages
				rs.mc.run();
				//regen
				if (rs.rc.getType() == RobotType.SCOUT && rs.rc.getFlux() >= GameConstants.REGEN_COST)
					rs.hc.run();
				//attack
				if (!rc.isAttackActive() && rs.rc.getType() != RobotType.ARCHON)
					rs.ac.run();

				//flux
				rs.fc.run();
				//spawn/movement
				if (!rc.isMovementActive())
					rs.nc.run();
				
				rc.yield();
			} catch (Exception e) {
				System.out.println("caught exception:");
				e.printStackTrace();
			}
		}
	}
}
