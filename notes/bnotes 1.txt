Globals:
-Owned Powernodes
-Round
-Allied Archons
-Adjacent Powernodes

RobotPlayer:
-sense
-receive messaging
-transmit messaging //occurs at end of round
-determine state
-transfer flux
-attack
-movement
	-spawn
	-move
	-set direction

Classes:
*MessageController
*FluxController
*AttackController
	-always attack if possible
	-target weakest? closest? farthest? soldiers/scouts/etc?
*SpawnController
	-when to spawn
	-how to spawn
		(spawn one turn, set direction the other)
*NavigationController/MoveController
	-bugging (.bugTo)
	-dijkstra's/a*/d* (.revisitTo, .queueTo)
	-positioning in fight
		-kite (scouts)
		-minimize #enemy units that can attack it
		-run/push
	-where to move to
		-power nodes
		-enemy forces
		-enemy archons
	

States:
*SpawnController
	-expand
	-units
	-towers
*MoveController
	-patrolling
	-movingTo
	-scouting
	-groupingTo
	-fighting
		-dogfight
		-run
		-kite
		-push

Utility Classes:
	-HashSet
	-FastRobotInfo
	-InfoCache